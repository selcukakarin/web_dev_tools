var express = require("express")
var app = express()
var bodyParser = require("body-parser")

var urlEncodedParser = bodyParser.urlencoded({extended:false})

app.get("/", function (request, response) {
    response.send("Selam node!")
})

app.get("/admin", function (request, response) {
    response.send("Selam admin express!!!")
})

app.get("/product", function (request, response) {
    console.log("ID : " + request.query.id)
    console.log("Kategori ID : " + request.query.categoryid)
    response.send("Selam product!")
})

app.get("/product", function (request, response) {
    response.send("Selam product!")
})

app.post("/product", urlEncodedParser, function (request, response) {
    console.log(request.body.isim)
    response.send("Post isteği alındı")
})

app.delete("/product", function (request, response) {
    response.send("Delete isteği alındı")
})

app.put("/product", function (request, response) {
    response.send("put isteği alındı")
})

app.get("/customer*", function (request, response) {
    response.send("Müşteriler")
})

app.get("/*customer*", function (request, response) {
    response.send("Müşteriler geldi")
})

var server = app.listen(8080);